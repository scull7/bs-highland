module Readable = struct
  type 'a t
end

module Writeable = struct
  type 'a t
end

module Duplex = struct
  type ('a, 'b) t

  external to_readable : ('a, 'b) t -> 'a Readable.t = "%identity"

  external to_writeable : ('a, 'b) t -> 'b Writeable.t = "%identity"
end

module Terminal = struct
  type 'a t

  external on : 
    'a t ->
    (
      [
      | `close of unit -> unit
      | `finish of unit -> unit
      | `error of exn -> unit
      ]
      [@bs.string]
    ) ->
    'a t
    = "on"
    [@@bs.send]
    
end
