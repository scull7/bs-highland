module NodeStream = Highland_node_stream


type 'a t

include Highland_constructor.Make(struct type 'a stream = 'a t end)

(*
 * ## Transforms
 *)

external doto : 'a t -> ('a -> unit) -> 'a t = "" [@@bs.send]

external map : 'a t -> ('a -> 'b) -> 'b t = "" [@@bs.send]

external through : 'a t -> ('a t -> 'b t) -> 'b t = "" [@@bs.send]


(*
 * ## Consumption
 *)
external finish : 'a t -> (unit -> unit) -> unit = "done" [@@bs.send]

external toArray : 'a t -> ('a array -> unit) -> unit = "toArray" [@@bs.send]


(*
 * ## Higher-Order Streams
 *)
external concat : 'a t -> 'a t -> 'a t = "concat" [@@bs.send]

external sequence : 'a t -> 'b t = "sequence" [@@bs.send]

external zip : 'a t -> 'b t -> ('a * 'b) t = "zip" [@@bs.send]

(*
 * ## Consumption
 *)
external pipe :
  'a t ->
  ('a, 'b) NodeStream.Duplex.t ->
  'b NodeStream.Writeable.t
  = "pipe" [@@bs.send]

external sink : 'a t -> 'a NodeStream.Writeable.t -> 'a NodeStream.Terminal.t
= "pipe" [@@bs.send]
