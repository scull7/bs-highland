module NStream = Highland_node_stream

external read : string -> Node.Buffer.t NStream.Readable.t = "createReadStream"
[@@bs.module "fs"]

external write : string -> Node.Buffer.t NStream.Writeable.t =
"createWriteStream" [@@bs.module "fs"]

external writeString : string -> string NStream.Writeable.t =
"createWriteStream" [@@bs.module "fs"]

external pipe :
  'a NStream.Readable.t ->
  ('a, 'b) NStream.Duplex.t ->
  'b NStream.Readable.t =
  "pipe" [@@bs.send]

external sink :
  'a NStream.Readable.t ->
  'a NStream.Writeable.t ->
  'a NStream.Terminal.t =
  "pipe" [@@bs.send]
