module NodeStream = Highland_node_stream


module type Stream = sig
  type 'a stream
end

module Make (Stream: Stream) = struct
  type 'a stream = 'a Stream.stream

  external make :
    (
      [
      | `Array of 'a array
      | `Node of 'a NodeStream.Readable.t
      ] [@bs.unwrap]
    ) -> 'a stream = "highland"
    [@@bs.module]
end
