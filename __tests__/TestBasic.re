open Jest;

describe("A few simple examples", () => {

  let squareAndSum = (s) => {
    let sum = ref(0);

    Highland.map(s, x => {
      let square = x * x;
      let y = sum^;
      sum := y + square;
      square;
    });
  };
  
  testAsync("Tracking state while iterating over a simple array", (finish) => {
    let outer_state = ref(0);

    Highland.make(`Array([|1, 2, 3, 4, 5|]))
    |. Highland.through(squareAndSum)
    |. Highland.doto(x => outer_state := outer_state^ + x)
    |. Highland.finish(() =>
      Expect.expect(outer_state^)
      |> Expect.toBe(55)
      |> finish
    );
  });

  testAsync("toArray", (finish) => {
    let outer_state = ref(0);

    Highland.make(`Array([|1, 2, 3, 4, 5|]))
    |. Highland.through(squareAndSum)
    |. Highland.doto(x => outer_state := outer_state^ + x)
    |. Highland.toArray((array) => {
      let sum = Belt.Array.reduce(array, 0, (acc, x) => x + acc); 
      Expect.expect(outer_state^)
      |> Expect.toBe(sum)
      |> finish
    });
  });
});

describe("Higher-Order Streams", () => {

  describe("concat", () => {

    testAsync("concat docs example", (finish) =>
      Highland.make(`Array([|1, 2|]))
      |. Highland.concat(Highland.make(`Array([|3, 4|])))
      |. Highland.toArray((array) =>
        Expect.expect(array)
        |> Expect.toEqual([|1, 2, 3, 4|])
        |> finish
      )
    );
  });

  describe("sequence", () => {

    testAsync("sequence docs example", (finish) =>
      Highland.make(`Array([|
        Highland.make(`Array([|1, 2, 3|])),
        Highland.make(`Array([|4, 5, 6|])),
      |]))
      |. Highland.sequence
      |. Highland.toArray((array) =>
        Expect.expect(array)
        |> Expect.toEqual([|1, 2, 3, 4, 5, 6|])
        |> finish
      )
    );
  });

  describe("zip", () => {
    testAsync("zip docs example", (finish) =>
      Highland.make(`Array([|'a', 'b', 'c'|]))
      |. Highland.zip(Highland.make(`Array([|1, 2, 3|])))
      |. Highland.toArray((array) =>
        Expect.expect(array)
        |> Expect.toEqual([| ('a', 1), ('b', 2), ('c', 3) |])
        |> finish
      )
    )
  });

  describe("read file", () => {
    testAsync("Test File Streams", (finish) => {
      let src = "/tmp/bs-highland-read-file.txt";
      let dest = "/tmp/bs-highland-read-file-dest.txt";
      Node.Fs.writeFileAsUtf8Sync(src, "Don");

      Highland_file.read(src)
      |. Highland_file.sink(Highland_file.write(dest))
      |. Highland_node_stream.Terminal.on(`finish(_ =>
        Node.Fs.readFileAsUtf8Sync(dest)
        |> Expect.expect
        |> Expect.toBe("Don")
        |> finish
      ))
      |. ignore
    })
  });

  describe("pipe", () => {
    let file = "/tmp/bs-highland-pipe.txt";
    testAsync("file write example", (finish) =>
      Highland.make(`Array([|"N", "a", "t", "e"|]))
      |. Highland.sink(Highland_file.writeString(file))
      |. Highland_node_stream.Terminal.on(`finish(_ =>
        Node.Fs.readFileAsUtf8Sync(file)
        |> Expect.expect
        |> Expect.toEqual("Nate")
        |> finish
      ))
      |. ignore
    )
  });
});
