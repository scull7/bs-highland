[![pipeline status](https://gitlab.com/scull7/bs-highland/badges/master/pipeline.svg)](https://gitlab.com/scull7/bs-highland/commits/master)

# bs-highland

BuckleScript bindings for the Highland JavaScript stream library.

## Usage
```reason
Highland.make( `Array([| 1, 2 |]) )
|. Highland.zip(Highland.make( `Array([| 3, 4 |]) ))
|. Highland.toArray((array) => Js.log2("Array: ", array))
/* => [ 1, 2, 3, 4 ] */
```

[highland]: https://highlandjs.org
